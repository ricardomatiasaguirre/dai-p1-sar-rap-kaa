//Las regiones, provincias y comunas deben estar almacenadas en un archivo js aparte 
//llamado "localidades.js" en variables de tipo 
//array con objetos en json con "idregion", "idprovincia", "idcomuna" y "nombre" según 
//corresponda, en la carpeta /js
//La funciones de cálculo de edad y cambio de las regiones/provincias debe estar bien 
//documentado (comentado

var Regiones = [
    { "id_region": "02", "nombre": "Los Lagos" },
    { "id_region": "03", "nombre": "Magallanes" },
    { "id_region": "04", "nombre": "Metropolitana" },
]

var Provincias = [
    
    { "id_region": "02", "id_provincia": "021", "nombre": "Llanquihue" },
    { "id_region": "02", "id_provincia": "022", "nombre": "Chiloé" },
    { "id_region": "02", "id_provincia": "023", "nombre": "Osorno" },

    { "id_region": "03", "id_provincia": "031", "nombre": "Magallanes" },
    { "id_region": "03", "id_provincia": "032", "nombre": "Antartica chilena" },
    { "id_region": "03", "id_provincia": "033", "nombre": "Tierra del fuego" },
    
    { "id_region": "04", "id_provincia": "041", "nombre": "Santiago" },
    { "id_region": "04", "id_provincia": "042", "nombre": "Melipilla" },
    { "id_region": "04", "id_provincia": "043", "nombre": "Cordillera" }
    
]

var Comunas = [
    { "id_region": "02", "id_provincia": "021","id_comuna": "02101", "nombre": "Puerto Montt" },
    { "id_region": "02", "id_provincia": "021","id_comuna": "02102", "nombre": "Frutillar" },
    
    { "id_region": "02", "id_provincia": "022","id_comuna": "02201", "nombre": "Castro" },
    { "id_region": "02", "id_provincia": "022","id_comuna": "02202", "nombre": "Ancud" },
    
    { "id_region": "02", "id_provincia": "023","id_comuna": "02301", "nombre": "Puerto Octay" },
    { "id_region": "02", "id_provincia": "023","id_comuna": "02302", "nombre": "Purranque" },

    
    { "id_region": "03", "id_provincia": "031","id_comuna": "03101", "nombre": "Punta Arenas" },
    { "id_region": "03", "id_provincia": "031","id_comuna": "03102", "nombre": "Río Verde" },
    
    { "id_region": "03", "id_provincia": "032","id_comuna": "03201", "nombre": "Cabo de Hornos" },
    { "id_region": "03", "id_provincia": "032","id_comuna": "03202", "nombre": "Antartica" },
    
    { "id_region": "03", "id_provincia": "033","id_comuna": "03301", "nombre": "Porvenir" },
    { "id_region": "03", "id_provincia": "033","id_comuna": "03302", "nombre": "Timankel" },

    
    { "id_region": "04", "id_provincia": "041","id_comuna": "04101", "nombre": "Quinta Normal" },
    { "id_region": "04", "id_provincia": "041","id_comuna": "04102", "nombre": "Santiago Centro" },
    
    { "id_region": "04", "id_provincia": "042","id_comuna": "04201", "nombre": "San Pedro" },
    { "id_region": "04", "id_provincia": "042","id_comuna": "04202", "nombre": "Curacaví" },
    
    { "id_region": "04", "id_provincia": "043","id_comuna": "04301", "nombre": "Puente Alto" },
    { "id_region": "04", "id_provincia": "043","id_comuna": "04302", "nombre": "Pirque" },
]